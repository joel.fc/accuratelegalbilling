
import 'package:testapp/src/models/state.dart';
import 'package:testapp/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State?> fetch() =>
      UserApiProvider().fetchData();

}
