
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:testapp/src/models/data_model.dart';
import 'package:testapp/src/models/state.dart';


class UserApiProvider {

  Future<State?> fetchData() async {
    final response = await rootBundle.loadString('assets/data.json');
    final data = await json.decode(response);
    // print(data+"sdgfdsgds");
    return State<DataModel>.success(DataModel.fromJson(data));

  }


}
