
import 'dart:async';
import 'package:testapp/src/models/data_model.dart';
import 'package:testapp/src/models/state.dart';
import 'package:testapp/src/utils/constants.dart';
import 'package:testapp/src/utils/object_factory.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class UserBloc extends Object implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<DataModel> _fetch =
  new StreamController<DataModel>.broadcast();


  // ignore: close_sinks

  // stream controller is broadcasting the  details

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  Stream<DataModel> get fetchResponse =>
      _fetch.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  fetch()async{
    loadingSink.add(true);

    State? state =
    await ObjectFactory().repository.fetch();

    if (state is SuccessState) {
      loadingSink.add(false);
      _fetch.sink.add(state.value);

    } else if (state is ErrorState) {
      loadingSink.add(false);
      _fetch.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading.close();
    _fetch.close();
  }
}


