import 'package:flutter/material.dart';
import 'package:testapp/src/bloc/user_bloc.dart';
import 'package:testapp/src/models/data_model.dart';
import 'package:testapp/src/screens/dashboard.dart';
import 'package:testapp/src/utils/constants.dart';
import 'package:testapp/src/utils/object_factory.dart';
import 'package:testapp/src/utils/utils.dart';
import 'package:testapp/src/widgets/long_button.dart';
import 'package:testapp/src/widgets/text_field.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController userTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();
  bool check = false;
  bool validate = false;
  late DataModel _dataModel;
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(ObjectFactory().appHive.getUserId().toString()+"safs");
    if((ObjectFactory().appHive.getUserId().toString()!="null")&&(ObjectFactory().appHive.getUserPassword().toString()!="null"))
      {
        userTextController.text=ObjectFactory().appHive.getUserId();
        passwordTextController.text=ObjectFactory().appHive.getUserPassword();
      }
    userBloc.fetch();
    userBloc.fetchResponse.listen((event) {
      setState(() {
        _dataModel = event;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[2],
      body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
            children: [
              Center(
                  child: Text(
                "Log in",
                style: TextStyle(
                    color: Constants.kitGradients[3],
                    fontSize: screenHeight(context, dividedBy: 18),
                    fontWeight: FontWeight.bold),
              )),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Row(
                children: [
                  Text(
                    "Username",
                    style: TextStyle(
                        color: Constants.kitGradients[6], fontSize: 16),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: TextInput(
                    controller: userTextController,
                    hint: 'Username',
                    obscure: false,
                  )),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Row(
                children: [
                  Text(
                    "Password",
                    style: TextStyle(
                        color: Constants.kitGradients[6], fontSize: 16),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: TextInput(
                    controller: passwordTextController,
                    hint: 'Password',
                    obscure: true,
                  )),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Checkbox(
                    value: check,
                    onChanged: (bool? value) {
                      setState(() {
                        check = value!;
                      });
                    },
                  ),
                  Text(
                    'Remember me',
                    style: TextStyle(
                        fontSize: 16, color: Constants.kitGradients[5]),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              LongButton(
                  title: "Log in",
                  onTap: () {
                    if(userTextController.text.isEmpty||passwordTextController.text.isEmpty)
                      {
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          backgroundColor: Colors.black,
                          content: Text("Enter Credentials"),
                        ));
                      }
                    else {
                      for (int i = 0; i < _dataModel.credentials.length; i++) {
                        if (_dataModel.credentials[i].username ==
                            userTextController.text &&
                            _dataModel.credentials[i].password ==
                                passwordTextController.text) {
                          validate = true;
                        }
                      }
                      if (validate == true) {
                        if(check==true)
                          {
                            ObjectFactory().appHive.putUserId(userId: userTextController.text);
                            ObjectFactory().appHive.putUserPassword(password: passwordTextController.text);
                            print(ObjectFactory().appHive.getUserId()+"sasfas");
                            push(context, Dashboard(userId: userTextController.text,));
                          }
                        else {
                          ObjectFactory().appHive.putUserPassword(password: "null");
                          ObjectFactory().appHive.putUserId(userId: "null");
                          push(context, Dashboard(userId: userTextController.text,));
                        }
                      }
                      else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              backgroundColor: Colors.black,
                              content: Text("Invalid Credentials"),
                            ));
                        userTextController.clear();
                        passwordTextController.clear();
                      }
                    }
                  }),
            ],
        ),
      ),
          )),
    );
  }
}
