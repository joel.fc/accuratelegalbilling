import 'package:flutter/material.dart';
import 'package:testapp/src/screens/login_page.dart';
import 'package:testapp/src/utils/constants.dart';
import 'package:testapp/src/utils/object_factory.dart';
import 'package:testapp/src/utils/utils.dart';


class Dashboard extends StatefulWidget {
  final String userId;
   Dashboard({required this.userId});
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[2],
      body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: (){
                          push(context,const LoginPage());
                        },
                          child: Icon(Icons.arrow_back,color: Constants.kitGradients[3],size: screenHeight(context,dividedBy: 25),)
                      )
                    ],
                  ),
                  Center(child: Text("Welcome",style: TextStyle(color: Constants.kitGradients[3],fontSize: screenHeight(context,dividedBy: 24),fontWeight: FontWeight.bold),)),
                  SizedBox(height: screenHeight(context,dividedBy: 30),),
                  Text(widget.userId.isEmpty?ObjectFactory().appHive.getUserId():widget.userId,style: TextStyle(color: Constants.kitGradients[3],fontSize: screenHeight(context,dividedBy: 18),fontWeight: FontWeight.bold),),
                  SizedBox(height: screenHeight(context,dividedBy: 30),),

                ],
              ),
            ),
          )
      ),
    );
  }
}
