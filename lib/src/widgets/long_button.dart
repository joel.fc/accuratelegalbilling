import 'package:flutter/material.dart';
import 'package:testapp/src/utils/constants.dart';
import 'package:testapp/src/utils/utils.dart';


class LongButton extends StatelessWidget {
  final Function onTap;
  final String title;
   const LongButton({required this.title,required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        onTap();
      },
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Constants.kitGradients[5]),
        height: screenHeight(context, dividedBy: 13),
        width: screenWidth(context, dividedBy: 1),
        child: Center(
          child: Text(title,style: TextStyle(color: Constants.kitGradients[0],fontSize: 15),
          ),
        ),
      ),
    );
  }
}
