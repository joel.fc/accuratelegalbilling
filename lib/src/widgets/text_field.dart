import 'package:flutter/material.dart';
import 'package:testapp/src/utils/constants.dart';


class TextInput extends StatefulWidget {
  const TextInput({Key? key,required this.controller,required this.hint,required this.obscure}) : super(key: key);
  final String hint;
  final TextEditingController controller;
  final bool obscure;

  @override
  _TextInputState createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      style: TextStyle(color: Constants.kitGradients[6],fontSize: 14),
      obscureText: widget.obscure,
      decoration: InputDecoration(
          border: InputBorder.none,
          hintText: widget.hint,
          hintStyle: TextStyle(color: Constants.kitGradients[6],fontSize: 14)
      ),
    );
  }
}
