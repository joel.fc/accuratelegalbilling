import 'constants.dart';
import 'package:hive/hive.dart';

class AppHive {
  // void main (){}

  // void openBox()async{
  //   await Hive.openBox(Constants.BOX_NAME);
  //
  // }
  static const String _USER_ID = "user_id";
  static const String _PASSWORD = "password";

  void hivePut({String? key, String? value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGet({required String key}) {
    // openBox();
    print(Hive.box(Constants.BOX_NAME).get(key).toString()+"gng");
    return Hive.box(Constants.BOX_NAME).get(key).toString();
  }


  putUserId({required String userId}) {
    hivePut(key: _USER_ID, value: userId);
  }

  String getUserId() {
    // print(hiveGet(key: _USER_ID).toString()+"avb");
    return hiveGet(key: _USER_ID).toString();
  }


  putUserPassword({required String password}) {
    hivePut(key: _PASSWORD, value: password);
  }

  String getUserPassword() {
    return hiveGet(key: _PASSWORD);
  }

  AppHive();


}