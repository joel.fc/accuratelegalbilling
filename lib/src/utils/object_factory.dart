



import 'package:testapp/src/resources/repository/repository.dart';

import 'hive.dart';

/// it is a hub that connecting pref,repo,client
/// used to reduce imports in pages
class ObjectFactory {
  static final _objectFactory = ObjectFactory._internal();

  ObjectFactory._internal();



  factory ObjectFactory() => _objectFactory;

  ///Initialisation of Objects
  AppHive _appHive = AppHive();
  Repository _repository = Repository();




  ///
  /// Getters of Objects
  ///


  AppHive get appHive => _appHive;

  Repository get repository => _repository;




///
/// Setters of Objects
///

}
