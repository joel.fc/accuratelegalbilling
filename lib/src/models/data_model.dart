// To parse this JSON data, do
//
//     final dataModel = dataModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

DataModel dataModelFromJson(String str) => DataModel.fromJson(json.decode(str));

String dataModelToJson(DataModel data) => json.encode(data.toJson());

class DataModel {
  DataModel({
    required this.credentials,
  });

  final List<Credential> credentials;

  factory DataModel.fromJson(Map<String, dynamic> json) => DataModel(
    credentials: List<Credential>.from(json["credentials"].map((x) => Credential.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "credentials": List<dynamic>.from(credentials.map((x) => x.toJson())),
  };
}

class Credential {
  Credential({
    required this.username,
    required this.password,
  });

  final String username;
  final String password;

  factory Credential.fromJson(Map<String, dynamic> json) => Credential(
    username: json["username"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "username": username,
    "password": password,
  };
}
