import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';
import 'package:testapp/src/screens/dashboard.dart';
import 'package:testapp/src/screens/login_page.dart';
import 'package:testapp/src/utils/constants.dart';
import 'package:testapp/src/utils/object_factory.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  Hive.init(appDocDirectory.path);
  await Hive.openBox(Constants.BOX_NAME);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}
